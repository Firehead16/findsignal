import matplotlib.pyplot as plt
import pylab
with open('sig.txt') as f:
    signal = f.readlines()
signal_parameters = signal[0]
T = float(signal_parameters.split("  ", 1)[0][2:])
DT = float(signal_parameters.split("  ", 1)[1][3:])
rep = round(T/DT)
signal.remove(signal[0])
with open('dan.txt') as f:
    data = f.readlines()
N = int(data[0][2:])
data.remove(data[0])
power = float()
for el in signal:
    power += float(el) ** 2 #мощность сигнала
shift = list()
for i in range(0, N-rep):
    shift.append(0.0)
    for k in range(0, rep):
        shift[i] += float(signal[k]) * float(data[k + i]) #перемножение со сдвигом

filtered = list()
results = list()
filtered.append(shift[0])
m = int(input())
for i in range(0, len(shift)):
    filtered.append(filtered[i-1] + shift[i])
    if i >= m:
        filtered[i] -= 2 * shift[i-m]
    if i >= 2 * m:
        filtered[i] += shift[i - 2 * m]
    if (shift[i] > 0.8 * power and filtered[i] < 0 and filtered[i - 1] > 0):
        results.append(((i - 1) * filtered[i] - i * filtered[i - 1]) / (filtered[i] - filtered[i - 1]) - m)
        print(results[len(results)-1])
pylab.figure(1)
plt.plot(shift, 'ro')

pylab.figure(2)
plt.plot([i for i in range(0, 100)], i, 'o')
#plt.xticks([])
#plt.yticks([])
print(min(signal))
print(max(signal))
for i, el in enumerate(signal):
    print(signal[i])
pylab.figure(3)
plt.plot([i for i in range(0, 1000)], data, 'o')
plt.show()